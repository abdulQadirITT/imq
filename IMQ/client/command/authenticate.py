import re
from constants import constant
from exceptions.commandInvalidException import CommandInvalidException


class Authenticator:
    def iscommandCorrect(self, command):
        try:
            if re.search(constant.PUBLISH_COMMAND_VALIDATOR, command) is not None:
                return constant.PUBLISHER_COMMAND
            elif re.search(constant.SUBSCRIBE_COMMAND_VALIDATOR, command) is not None:
                return constant.SUBSCRIBER_COMMAND
            elif re.search(constant.SHOW_TOPICS_COMMAND_VALIDATOR, command) is not None:
                return constant.SHOW_TOPICS
            elif re.search(constant.EXIT_COMMAND_VALIDATOR, command) is not None:
                return constant.EXIT_COMMAND
            else:
                return False
        except CommandInvalidException:
            print('Command is Invalid')
