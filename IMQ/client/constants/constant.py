DECODE = 'utf-8'
byte = 1024

SUBSCRIBE = 'Subscribe'
PUBLISHER_COMMAND = 'publisherCommand'
SUBSCRIBER_COMMAND = 'subscriberCommand'
SHOW_TOPICS = 'showTopics'
INVALID_COMMAND = 'command is wrong'

# Main.py
WAITING_FOR_CONNECTION = 'Waiting for connection'
SOCKET_CONFIGURATION_FILE = "socketConfiguration.properties"
WELCOME_MESSAGE = 'Welcome to IMQ'

# Publisher.py
REQUEST_TYPE_PUBLISHER = 'publish'
MESSAGE_STATUS_PUBLISHER = 'messageStatus'

# Subscriber.py
REQUEST_TYPE_SUBSCRIBE = 'subscribe'
TOPIC_MESSAGES = 'topicMessages'
REQUEST_TYPE_SHOW_TOPICS = 'getTopics'
TOPIC_NAME = 'topicName'
AVAILABLE_TOPICS = '------------\nAll Available Topics\n------------'

REQUEST_TYPE = "connect_to_topic"
FORMAT = 'json'
DATA = ' '
MESSAGE_STATUS = 'Delivered'

filePath = 'F:\L&C IMQ Proj\Week2Copy\\'
openMethod = 'rb'

PUBLISH_COMMAND_VALIDATOR = 'imq publish [\D]* -m [\w]*'
SUBSCRIBE_COMMAND_VALIDATOR = 'imq subscribe [\D]*'
SHOW_TOPICS_COMMAND_VALIDATOR = 'imq show topics'
EXIT_COMMAND = 'imq exit'
EXIT_COMMAND_VALIDATOR = 'imq exit'
REQUEST_TYPE_EXIT = 'exit'
PUBLISHER_COMMAND = 'publisherCommand'
SUBSCRIBER_COMMAND = 'subscriberCommand'
SHOW_TOPICS = 'showTopics'
EIXT_IMQ = 'exitimq'

SERVER_CLOSED_EXCEPTION = 'Server Closed Abruptly'
COMMUNICATION_SERVER_ERROR = 'Error Occurred while communicating with Server'
SERVER_CLOSED_ABRUPTLY = 'Server is Down'
PARSING_EXCEPTION_ENCODING = 'Error in Json Encoding'
PARSING_EXCEPTION_DECODING = 'Error in Json Decoding'