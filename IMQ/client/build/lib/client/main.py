import socket
import sys
sys.path.append('../')
from fileOperation.readPropertyFile import ReadPropertyFile
import constants
from protocol.request import Request
from protocol.protocolParser import Parser


class Client:

    ClientSocket = socket.socket()

    def initiateConnection(self):
        print('Waiting for connection')
        configs = ReadPropertyFile.getConfig("socketConfiguration.properties")
        try:
            self.ClientSocket.connect((configs.get('host').data, int(configs.get('port').data)))
        except socket.error as errorMessage:
            print(str(errorMessage))

    def enterName(self):
        message = input('Enter Your Name: ')
        self.ClientSocket.send(str.encode(message))

    def chat(self):
        while True:
            message = input('Send Message: ')
            if message == "bye":
                self.ClientSocket.close()
                break

            requestHeader = Request(message)
            parsedHeader = Parser.getJsonEncoded(requestHeader)
            self.ClientSocket.send(str.encode(parsedHeader))
            # self.ClientSocket.send(str.encode(message))

            # Response = self.ClientSocket.recv(constants.byte)
            # print(Response.decode(constants.decode))
            Response = self.ClientSocket.recv(1024)
            parsedResponse = Parser.getJsonDecoded(Response.decode('utf-8'))
            responseMessage = parsedResponse["data"]


Client().initiateConnection()
Client().enterName()
Client().chat()
