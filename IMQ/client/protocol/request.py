import sys
sys.path.append('../')
from dataclasses import dataclass
# import constants
from constants import constant


@dataclass
class Request():
    topicName: str
    requestType: str = constant.REQUEST_TYPE
    data: str = constant.DATA
    version: int = 1
    format: str = constant.FORMAT