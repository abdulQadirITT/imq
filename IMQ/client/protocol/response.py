import sys
sys.path.append('../')
from dataclasses import dataclass
# import constants
from constants import constant


@dataclass
# class Response(ImqProtocol):
class Response():
    topicMessages: [] = []
    topicName: List[str] = field(default_factory=list)
    messageStatus: str = constant.MESSAGE_STATUS
    version: int = 1
    format: str = constant.FORMAT
