import jsonpickle
import json
from exceptions.parsingException import ParsingException
from constants import constant


class Parser:
    def getJsonEncoded(header):
        try:
            return json.dumps(jsonpickle.encode(header, unpicklable=False), indent=4)
        except ParsingException:
            print(constant.PARSING_EXCEPTION_ENCODING)

    def getJsonDecoded(header):
        try:
            header = json.loads(header)
            return json.loads(header)
        except ParsingException:
            print(constant.PARSING_EXCEPTION_DECODING)
