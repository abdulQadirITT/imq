import socket
import sys
sys.path.append('../')
from fileOperation.readPropertyFile import ReadPropertyFile
from constants import constant
# import constants
from protocol.request import Request
from protocol.protocolParser import Parser
from clientService import Service


class Client:

    ClientSocket = socket.socket()

    def initiateConnection(self):
        print(constant.WELCOME_MESSAGE)
        configs = ReadPropertyFile().getConfig(constant.SOCKET_CONFIGURATION_FILE)
        try:
            self.ClientSocket.connect((configs.get('host').data, int(configs.get('port').data)))
            return self.ClientSocket
        except socket.error as errorMessage:
            print(str(errorMessage))


clientSocket = Client().initiateConnection()
clientServiceObj = Service(clientSocket)
clientServiceObj.operation()
