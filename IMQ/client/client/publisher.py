import sys
sys.path.append('../')
from constants import constant
from protocol.request import Request
from protocol.protocolParser import Parser
from command.authenticate import Authenticator
from exceptions.serverClosedException import ServerClosedException


class Publish:
    def publisherOperation(self, clientSocket, command):
        try:
            jsonEncoded = Parser.getJsonEncoded(Request(
                                                data=' '.join(command[4:]),
                                                requestType=constant.REQUEST_TYPE_PUBLISHER,
                                                topicName=command[2]))
            clientSocket.send(str.encode(str(jsonEncoded)))

            responseMessage = clientSocket.recv(constant.byte)
            parsedResponse = Parser.getJsonDecoded(responseMessage.decode(constant.DECODE))
            print(parsedResponse[constant.MESSAGE_STATUS_PUBLISHER])

        except ServerClosedException:
            print(constant.SERVER_CLOSED_EXCEPTION)

        except Exception:
            print(constant.COMMUNICATION_SERVER_ERROR)
