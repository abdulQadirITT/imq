import sys
from publisher import Publish
from subscriber import Subscribe

sys.path.append('../')
from protocol.protocolParser import Parser
from constants import constant
from protocol.request import Request
from command.authenticate import Authenticator
from exceptions.commandInvalidException import CommandInvalidException


class Service:

    def __init__(self, clientSocket):
        self.clientSocket = clientSocket

    def operation(self):
        while True:
            try:
                command = input().lower().split()
                if Authenticator().iscommandCorrect(' '.join(command)) == constant.PUBLISHER_COMMAND:
                    Publish().publisherOperation(self.clientSocket, command)
                elif Authenticator().iscommandCorrect(' '.join(command)) == constant.SUBSCRIBER_COMMAND:
                    Subscribe().subscriberOperation(self.clientSocket, command)
                elif Authenticator().iscommandCorrect(' '.join(command)) == constant.SHOW_TOPICS:
                    Subscribe().showTopics(self.clientSocket, command)
                elif Authenticator().iscommandCorrect(' '.join(command)) == constant.EXIT_COMMAND:
                    jsonEncoded = Parser.getJsonEncoded(Request
                                                (requestType=constant.REQUEST_TYPE_EXIT, topicName=' '))
                    self.clientSocket.send(str.encode(str(jsonEncoded)))
                    sys.exit()
                else:
                    print(constant.INVALID_COMMAND)
            except CommandInvalidException:
                print('Invalid Command Entered')
