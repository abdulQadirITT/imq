import sys
sys.path.append('../')
from protocol.request import Request
from constants import constant
from protocol.protocolParser import Parser
from command.authenticate import Authenticator
from exceptions.serverClosedException import ServerClosedException


class Subscribe:
    def subscriberOperation(self, clientSocket, command):
        try:
            jsonEncoded = Parser.getJsonEncoded(Request
                                                (requestType=constant.REQUEST_TYPE_SUBSCRIBE,
                                                    topicName=command[2]))
            clientSocket.send(str.encode(str(jsonEncoded)))

            responseMessage = clientSocket.recv(constant.byte)
            parsedResponse = Parser.getJsonDecoded(responseMessage.decode(constant.DECODE))
            if len(parsedResponse[constant.TOPIC_MESSAGES]) != 0:
                print('----------')
                for message in parsedResponse[constant.TOPIC_MESSAGES]:
                    print('>>> ' + message)
            else:
                print(parsedResponse['messageStatus'])
        except ServerClosedException:
            print(constant.SERVER_CLOSED_EXCEPTION)
        except Exception:
            print(constant.SERVER_CLOSED_ABRUPTLY)

    def showTopics(self, clientSocket, command):
        try:
            jsonEncoded = Parser.getJsonEncoded(Request
                                                (requestType=constant.REQUEST_TYPE_SHOW_TOPICS,
                                                    topicName=command[1]))
            clientSocket.send(str.encode(str(jsonEncoded)))

            responseMessage = clientSocket.recv(constant.byte)
            parsedResponse = Parser.getJsonDecoded(responseMessage.decode(constant.DECODE))
            if len(parsedResponse[constant.TOPIC_NAME]) != 0:
                print(constant.AVAILABLE_TOPICS)
                for topic in parsedResponse[constant.TOPIC_NAME]:
                    print(topic)
            else:
                print(parsedResponse[constant.MESSAGE_STATUS])

        except ServerClosedException:
            print(constant.SERVER_CLOSED_EXCEPTION)

        except Exception as errors:
            print(error)
