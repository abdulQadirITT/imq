from jproperties import Properties
import sys
sys.path.append('../')
from os import path
from fileOperation import constants


class ReadPropertyFile:
    def getConfig(self, fileName):
        configs = Properties()
        filepath = path.relpath(constants.filePath + fileName)
        with open(filepath, constants.openMethod) as config_file:
            configs.load(config_file)
        return configs
