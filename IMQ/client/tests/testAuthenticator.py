import unittest
import sys
import os
sys.path.append('../')
from unittest.mock import Mock, create_autospec, patch
from command.authenticate import Authenticator


class testAuthenticator(unittest.TestCase):
    __commandParser : None

    def test_iscommandCorrect_publisher(self):
        self.__commandParser = Authenticator()
        result = self.__commandParser.iscommandCorrect('imq publish news -m hey')
        assert 'publisherCommand' == result

    def test_isCommandCorrectSubscriber(self):
        self.__commandParser = Authenticator()
        result = self.__commandParser.iscommandCorrect('imq subscribe news')
        assert 'subscriberCommand' == result

    def test_isCommandCorrectShowTopics(self):
        self.__commandParser = Authenticator()
        result = self.__commandParser.iscommandCorrect('imq show topics')
        assert 'showTopics' == result
    
    def test_isCommandCorrectWrongCommand(self):
        self.__commandParser = Authenticator()
        result = self.__commandParser.iscommandCorrect('imq show topi')
        assert False == result

