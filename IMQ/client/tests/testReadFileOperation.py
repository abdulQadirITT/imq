import unittest
import sys
import os
sys.path.append('../')
from unittest.mock import Mock, create_autospec, patch
from fileOperation.readPropertyFile import ReadPropertyFile


class TestReadPropertyFile(unittest.TestCase):
    __readFile : None
    def testingProperty(self):
        __readFile = ReadPropertyFile()
        results = __readFile.getConfig('socketConfiguration.properties')
        self.assertIsNotNone(results)

