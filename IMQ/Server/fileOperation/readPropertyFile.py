from jproperties import Properties
import sys
sys.path.append('../')
from os import path
# from fileOperation import constants
# from constants import constant


class ReadPropertyFile:
    def getConfig(self, fileName):
        configs = Properties()
        filepath = path.relpath('F:\L&C IMQ Proj\Week2Copy\\' + fileName)
        with open(filepath, 'rb') as config_file:
            configs.load(config_file)
        return configs
