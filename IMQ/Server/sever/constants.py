DECODE = 'utf-8'
byte=2048
SOCKET_CONFIGURATION_FILE = "socketConfiguration.properties"
WAITING_FOR_CONNECTION = 'Waiting for connection'
CONNETECTED_WITH = 'Connected to: '

# queueService.py
REQUEST_TYPE_PUBLISH = 'publish'
MESSAGE_STATUS = 'messageStatus'
REQUEST_TYPE = 'requestType'
REQUEST_TYPE_SUBSCRIBE = 'subscribe'
TOPIC_MESSAGES = 'topicMessages'
REQUEST_TYPE_SHOW_TOPICS = 'getTopics'
TOPIC_NAME = 'topicName'
DATA = 'data'
DELIVERED = 'Delievered'
TOPIC_DOES_NOT_EXIST = 'Topic does not Exist'