import sys
sys.path.append('../')
from database.saveController import SaveController


class Queue:
    topicName = []
    topicData = {
    }

    def __init__(self):
        self.topicName = SaveController().getAllTopics()
        for topic in self.topicName:
            self.topicData.update({topic: []})

    def set_topicData(self, name, value):
        self.topicData[name].append(value)

    def get_topicData(self, name):
        return self.topicData[name]

    def get_topicNames(self):
        return self.topicName
