import socket
import sys
from _thread import *
sys.path.append('../')
from fileOperation.readPropertyFile import ReadPropertyFile
# import serverService
from sever.serverService import ServerService
from queue import Queue
from sever.scheduler import Schedule
from Exceptions.bindSocketException import BindSocketException


class Server:

    ServerSocket = socket.socket()
    ThreadCount = 0
    connectClient = ServerService()

    def initiateServer(self):
        configs = ReadPropertyFile().getConfig("socketConfiguration.properties")
        try:
            self.ServerSocket.bind((configs.get('host').data, int(configs.get('port').data)))            
            print('Waiting for connection')
            self.ServerSocket.listen()
        except BindSocketException:
            print('Error in Inititializing the server')
        except socket.error as errorMessage:
            print(str(errorMessage))

    def runServer(self):
        try:
            queue = Queue()
            schedule = Schedule()
            start_new_thread(schedule.runScheduler, (queue,))
            while True:
                Client, address = self.ServerSocket.accept()
                start_new_thread(self.connectClient.serveNewClient, (Client, address, queue))
                print('Connected to: ' + address[0] + ':' + str(address[1]))
        except Exception:
            print('Error in Running the Server')


Server().initiateServer()
Server().runServer()
