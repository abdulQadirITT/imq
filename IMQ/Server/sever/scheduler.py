import schedule
import time
import datetime


class Schedule:

    def checkMessageExpiry():
        for topic in self.queue.topicData:
            messages = self.queue.topicData[topic]
            for message in messages:
                if message.expireTime > datetime.datetime.now():
                    messages.remove(message)

    def runScheduler(self, queue):
        try:
            schedule.every(10).minutes.do(self.checkMessageExpiry)
            while True:
                schedule.run_pending()
                time.sleep(1)
        except Exception:
            print('Dead Letter queue exception')
