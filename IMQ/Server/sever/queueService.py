import datetime
from datetime import timedelta
import sys
sys.path.append('../')
from protocol.response import Response
from protocol.protocolParser import Parser
from sever.message import Message
from Exceptions.requestHandlingException import RequestHandlingException
from Exceptions.unableToFetchTopicDataException import UnableToFetchTopicDataException
# import constants
# from constants import constant


class Service:
    def __init__(self, queue):
        self.queue = queue

    def authenticateTopic(self, topicName):
        if topicName not in self.queue.topicName:
            return False
        else:
            return True

    def publishData(self, topicName, data):
        topicMessage = Message()
        topicMessage.data = data
        topicMessage.createTime = datetime.datetime.now()
        topicMessage.expireTime = datetime.datetime.now() + timedelta(days=1)
        self.queue.set_topicData(topicName, topicMessage)

    def data(self, parsedHeader, connection):
        try:

            if self.authenticateTopic(parsedHeader['topicName']):

                if parsedHeader['requestType'] == 'publish':
                    self.publishData(parsedHeader['topicName'], parsedHeader['data'])
                    connection.sendall(str.encode(str(Parser().getJsonEncoded(
                                        Response(messageStatus='Delivered')))))

                elif parsedHeader['requestType'] == 'subscribe':
                    messages = self.pullData(parsedHeader['topicName'])
                    if len(messages) == 0:
                        connection.sendall(str.encode(str(Parser().getJsonEncoded(
                                        Response(messageStatus='No Message In Queue')))))
                    else:
                        connection.sendall(str.encode(str(Parser().getJsonEncoded(
                                        Response(topicMessages=messages)))))

            elif parsedHeader['requestType'] == 'getTopics':
                topics = self.queue.get_topicNames()
                connection.sendall(str.encode(str(Parser().getJsonEncoded(
                                    Response(topicName=topics)))))

            elif parsedHeader['requestType'] == 'exit':
                connection.close()

            else:
                connection.sendall(str.encode(str(Parser().getJsonEncoded(
                                        Response(messageStatus='Topic does not Exist')))))

        except RequestHandlingException:
            print('Error Occurred while handling the Client Data')

    def pullData(self, topicName):
        try:
            topicMessages = []
            if self.authenticateTopic(topicName):
                for messages in self.queue.get_topicData(topicName):
                    topicMessages.append(messages.data)
                return topicMessages
            else:
                return False
        except UnableToFetchTopicDataException:
            print('Error in Fetching Data for the topic')
