import sys
sys.path.append('../')

from saveMethods.save import SaveData
from protocol.response import Response
from protocol.protocolParser import Parser
from database.saveController import SaveController
from sever.queueService import Service
from Exceptions.clientLeaveException import ClientLeaveException


saveData = SaveData()


class ServerService:
    def serveNewClient(self, connection, address, queue):
        try:
            while True:
                SaveController().saveloggedInUserDetails(address)
                data = connection.recv(2048)
                parsedHeader = Parser().getJsonDecoded(data.decode('utf-8'))
                Service(queue).data(parsedHeader, connection)
            connection.close()
        except ClientLeaveException:
            print(str(address[0]) + str(address[1])+' Client left')
        except Exception:
            print(str(address[0]) + str(address[1]) +' Client Left ')
