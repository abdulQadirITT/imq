import datetime


class Message:
    data: str
    expireTime: datetime.datetime
    createTime: datetime.datetime
    author: str
