import sys
sys.path.append('../')

from saveMethods.save import SaveData
import constants
from protocol.response import Response
from protocol.protocolParser import Parser


saveData = SaveData()


class ServerService:
    def serveNewClient(self, connection, address):
        clientName = connection.recv(constants.byte)
        while True:
            data = connection.recv(constants.byte)
            parsedMessage = Parser().getJsonDecoded(data.decode('utf-8'))
            data = parsedMessage['data']
            # reply = 'Server Says: ' + data.decode(constants.decode)
            reply = 'Server Says: ' + data
            if not data:
                break
            
            responseHeader = Response(reply)
            parsedHeader = Parser().getJsonEncoded(responseHeader)            
            saveData.saveDataToDB(data, address)
            connection.sendall(str.encode(parsedHeader))
        connection.close()
