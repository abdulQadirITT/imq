import socket
import sys
from _thread import *
sys.path.append('../')
from fileOperation.readPropertyFile import ReadPropertyFile
import serverService


class Server:

    ServerSocket = socket.socket()
    ThreadCount = 0
    connectClient = serverService.ServerService()    

    def initiateServer(self):
        configs = ReadPropertyFile.getConfig("socketConfiguration.properties")
        try:
            self.ServerSocket.bind((configs.get('host').data, int(configs.get('port').data)))            
            print('Waitiing for a Connection..')
            self.ServerSocket.listen()
        except socket.error as errorMessage:
            print('error')
            print(str(errorMessage))

    def runServer(self):
        while True:                        
            Client, address = self.ServerSocket.accept()
            start_new_thread(self.connectClient.serveNewClient, (Client, address))
            self.ThreadCount += 1            
            print('Thread Number: ' + str(self.ThreadCount))
            print('Connected to: ' + address[0] + ':' + str(address[1]))


Server().initiateServer()
Server().runServer()
