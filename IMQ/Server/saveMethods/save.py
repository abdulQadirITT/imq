import sys
sys.path.append('../')
from database.saveController import SaveController
import constants


class SaveData:
    def saveDataToFile(self, data, address):
        fileName = str(address[0]) + " " + str(address[1])
        file = open(fileName, constants.openMethod)
        file.write("\n" + data.decode(constants.decode))

    def saveDataToDB(self, data, address):
        SaveController.saveData(data, address)
