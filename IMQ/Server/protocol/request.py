import sys
sys.path.append('../')
from dataclasses import dataclass


@dataclass
class Request():
    data: str
    topicName: str
    requestType: str = "connect_to_topic"
    version: int = 1
    format: str = 'json'
