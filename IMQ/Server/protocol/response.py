# import constants
import sys
sys.path.append('../')
from dataclasses import dataclass, field
from typing import List
# import constants
# from constants import constant


@dataclass
class Response():
    topicMessages: List[str] = field(default_factory=list)
    topicName: List[str] = field(default_factory=list)
    messageStatus: str = 'Delivered'
    version: int = 1
    format: str = 'json'
