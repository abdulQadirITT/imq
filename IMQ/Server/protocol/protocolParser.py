import jsonpickle
import json


class Parser:
    def getJsonEncoded(self, header):
        return json.dumps(jsonpickle.encode(header, unpicklable=False), indent=4)

    def getJsonDecoded(self, header):
        headers = json.loads(header)
        return json.loads(headers)
