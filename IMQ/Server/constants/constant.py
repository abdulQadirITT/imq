CONNECTION_ALREADY_EXIST = 'One Connection already Exists'
DB_CONFIGURATION_PROPERTIES = 'dbConfigure.properties'
DEFAULT_IP = '192.0.0.0'
IS_TOPIC_EXIST_QUERY = 'select id from topic where name = %s'
GET_TOPIC_QUERY = 'select name from topic'
INSERT_LOGGED_IN_USER_DETAILS = 'insert into loggedInDetail values (%s, %s)'
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

filePath = 'F:\L&C IMQ Proj\Week2Copy\\'
openMethod = 'rb'

REQUEST_TYPE = "connect_to_topic"
DATA = ' '
MESSAGE_STATUS = 'Delivered'
FORMAT = 'json'

DECODE = 'utf-8'
byte=2048
SOCKET_CONFIGURATION_FILE = "socketConfiguration.properties"
WAITING_FOR_CONNECTION = 'Waiting for connection'
CONNETECTED_WITH = 'Connected to: '

# queueService.py
REQUEST_TYPE_PUBLISH = 'publish'
MESSAGE_STATUS = 'messageStatus'
REQUEST_TYPE = 'requestType'
REQUEST_TYPE_SUBSCRIBE = 'subscribe'
TOPIC_MESSAGES = 'topicMessages'
REQUEST_TYPE_SHOW_TOPICS = 'getTopics'
TOPIC_NAME = 'topicName'
DATA = 'data'
DELIVERED = 'Delievered'
TOPIC_DOES_NOT_EXIST = 'Topic does not Exist'