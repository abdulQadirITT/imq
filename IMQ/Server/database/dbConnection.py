import sys
sys.path.append('../')

from fileOperation.readPropertyFile import ReadPropertyFile
import mysql.connector


class DbConnection:
    __instance = None
    @staticmethod
    def getInstance():
        if DbConnection.__instance == None:
            DbConnection()
        return DbConnection.__instance

    def __init__(self):
        if DbConnection.__instance != None:
            raise Exception('One Connection already Exists')
        else:
            configs = ReadPropertyFile().getConfig('dbConfigure.properties')
            connection = mysql.connector.connect(
                host=configs.get('host').data,
                user=configs.get('user').data,
                password=configs.get('password').data,
                database=configs.get('database').data,
            )
            DbConnection.__instance = connection
