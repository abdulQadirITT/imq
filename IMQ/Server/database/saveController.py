import sys
sys.path.append('../')

from database.saveService import SaveService
import MySQLdb as dbExceptions
from Exceptions.savingLoggedInUserException import SavingLoggedInUserException
from Exceptions.getAllTopicException import GetAllTopicException


class SaveController:
    # def saveData(data, address):
    #     try:
    #         connection = SaveService.getDBConnection()
    #         if len(SaveService.IsUserInRootTable(address, connection)) == 0:
    #             SaveService.enterUserInRootTable(address, connection)
    #         SaveService.insertData(data, address, connection)

    #     except Exception as message:
    #         print(str(message))

    def saveloggedInUserDetails(self, address):
        try:
            saveServiceObj = SaveService(address)
            saveServiceObj.enterUserLoggedInDetails()
        except SavingLoggedInUserException:
            print('Unable to Save Logged In User Details')

    # def operation(self, data, topicName, address):
    #     saveServiceObj = SaveService(address)
    #     try:
    #         if len(saveServiceObj.isTopicExist(topicName)) == 0:
    #             saveServiceObj.insertData(data, topicName)
    #     except Exception as message:
    #         print(str(message))

    def getAllTopics(self):
        try:
            records = SaveService('192.0.0.0').getTopics()
            topics = []
            for row in records:
                topics.append(row[0])

            return topics
        except GetAllTopicException:
            print('Unable to retrieve Topics from Database')
