import sys
sys.path.append('../')
import time
import datetime
from jproperties import Properties
from database.dbConnection import DbConnection
# from constants import *
# from constants import constant


class SaveService:

    def __init__(self, address):
        self.connection = DbConnection.getInstance()
        self.address = address

    def isTopicExist(self, topicName):
        cursor = self.connection.cursor()
        query = 'select id from topic where name = %s'
        cursor.execute(query, (topicName,))
        return cursor.fetchall()

    def getTopics(self):
        cursor = self.connection.cursor()
        query = 'select name from topic'
        cursor.execute(query)
        return cursor.fetchall()

    def enterUserLoggedInDetails(self):
        cursor = self.connection.cursor()
        query = 'insert into loggedInDetail values (%s, %s)'
        cursor.execute(query, (self.address[1], datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.connection.commit()

    # def IsUserInRootTable(address, connection):
    #     cursor = connection.cursor()
    #     query = "select * from rootTable where clientName = %s"
    #     clientName = address[0] + ':' + str(address[1])
    #     cursor.execute(query, (clientName,))
    #     return cursor.fetchall()

    # def enterUserInRootTable(address, connection):
    #     cursor = connection.cursor()
    #     serialId = SaveService.getSerialId(connection)
    #     insertQuery = 'insert into roottable values (%s, %s)'
    #     cursor.execute(insertQuery, (serialId, address[0] + ':' + str(address[1]), ))
    #     connection.commit()

    # def getSerialId(connection):
    #     cursor = connection.cursor()
    #     selectQuery = "select id from roottable order by id desc limit 1"
    #     cursor.execute(selectQuery)
    #     return cursor.fetchone()[0] + 1

    # def insertData(self, data, topicName):
    #     # currentTime = time.strftime("%H:%M:%S", time.localtime())
    #     cursor = self.connection.cursor()
    #     query = "insert into messages (data, topicName) values (%s, %s)"
    #     cursor.execute(query, (data, topicName, self.address[0] + ':' + str(self.address[1]), ))
    #     connection.commit()