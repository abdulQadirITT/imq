import socket
import sys
from _thread import *
import os
sys.path.append(os.path.abspath(__file__) + "/../..")
# sys.path.append('../')
import unittest
from unittest.mock import patch
from sever.queue import Queue


class TestQueue(unittest.TestCase):
    def test_getTopic(self):
        queues = Queue()
        emptyList = []
        self.assertEqual(emptyList, queues.get_topicData('news')) 

    def test_getTopicNames(self):
        queue = Queue()
        self.assertEqual(3, len(queue.get_topicNames()))
