import unittest
import sys
import os
sys.path.append('../')
from unittest.mock import Mock, create_autospec, patch
from database.saveController import SaveController


class TestSaveController(unittest.TestCase):

    def testSave(self):
        self.assertEqual(3, len(SaveController().getAllTopics()))
