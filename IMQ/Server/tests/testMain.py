import sys
import os
import unittest
sys.path.append(os.path.abspath(__file__) + "/../..")
from testDatabase import *
from testParser import *
from testQueues import *
from testReadFileOperation import *

if __name__ == '__main__':
    unittest.main()
