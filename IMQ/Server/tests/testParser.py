import socket
import sys
from _thread import *
import os
# sys.path.append('../')
sys.path.append(os.path.abspath(__file__) + "/../..")
import unittest
from unittest.mock import patch
from protocol.protocolParser import Parser


class TestParser(unittest.TestCase):
    def testEncoding(self):
        self.assertEqual("\"\\\"test\\\"\"", Parser().getJsonEncoded('test'))

    def testDecoding(self):
        self.assertEqual('test', Parser().getJsonDecoded("\"\\\"test\\\"\""))
